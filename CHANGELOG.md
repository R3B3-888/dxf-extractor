# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2022-06-03

### Added

- button to convert in dwg using ODA only in windows

## [1.0.1] - 2022-05-25

### Changed

- Link dxf-cc for georeference
- Output in a `_DXF` folder

## [1.0.0-alpha] - 2022-05-25

### Added

- All the extraction is working
- popup window when an error occured or when the downloading is finish
- Benauge.csv to test with concret example

### Changed

- README roadmap updated

## [0.1.0] - 2022-05-13

### Added

- Compilation instruction for Windows
- Compilation instruction for Linux
- Connect all the buttons with actions
- Installer for windows
- LICENCE

### Changed

- Command Line integration to graphic

## [0.0.1] - 2022-05-08

### Added

- Graphical User Interface
- 3 Regions in the interface
- README explaining the purpose off the project and the idea behind
- CHANGELOG with the no link to Unreleased

[Unreleased]: https://gitlab.com/R3B3-888/dxf-extractor/-/compare/v1.1.0...main
[1.1.0]: https://gitlab.com/R3B3-888/dxf-extractor/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/R3B3-888/dxf-extractor/-/compare/v1.0.0-alpha...v1.0.1
[1.0.0-alpha]: https://gitlab.com/R3B3-888/dxf-extractor/-/compare/v0.1.0...v1.0.0-alpha
[0.1.0]: https://gitlab.com/R3B3-888/dxf-extractor/-/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/R3B3-888/dxf-extractor/-/tags/v0.0.1
