# DXF-Extractor

Application helping downloading zip files and extracting the dxf from it.
DXF files are cadastres from https://cadastre.data.gouv.fr/

## Visuals

![screenshot](img/screen_v001.png)

## Installation

### Using conda

Just run the following command :

```bash
conda env create --file environment.yml
```

## Compilation

### On linux

```shell
pyinstaller src/main.py
```

### On Windows

Remove the build/ and dist/ folders and the spec file to run the following command

```shell
pyinstaller -F -w --log-level=WARN --icon="img\dxf.ico" --name="DXF-Extractor" "src\main.py"
```

## Usage

You have 3 main area:

- "Necessary Informations": Specify the departement number
- "File paths": Specify the input csv file you want according to the [template](Input/Template.csv)
- "Buttons":
  - Download the zip
  - Extract only the specified cities you want and put it in a tree architecture in the specified path provided just above or put it by default in Output/

## Support

For any reclamation and bug report, please address at hoffmann.itconsulting@gmail.com

## Roadmap

Change the way the downloading is done by download only the city zip wanted.

And also, change the way of input the city through an inside table with flexible rows.

## Contributing

For now, contributions are allowed with PR.

## License

This repository is under GPL-3 License

## Project status

This project is under active developpement.
