import csv
import os
import subprocess
import sys
import tarfile
import zipfile

from PySide6 import QtWidgets, QtCore
import wget


class Dialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent=parent)
        self.resize(400, 150)

        self._create_form_group_box()
        self._create_file_paths_group_box()
        self._create_buttons_group_box()

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(self.form_group_box)
        main_layout.addWidget(self.file_paths_group_box)
        main_layout.addWidget(self.horizontal_group_box)
        self.setLayout(main_layout)
        self.setWindowTitle("DXF-Handler")

    def _create_form_group_box(self):
        self.form_group_box = QtWidgets.QGroupBox("Informations Nécessaires")
        layout = QtWidgets.QFormLayout()
        self.spin_box = QtWidgets.QSpinBox()
        self.spin_box.setRange(1, 95)
        self.spin_box.setValue(33)
        layout.addRow(
            QtWidgets.QLabel("Numéro du département:"), self.spin_box
        )
        self.form_group_box.setLayout(layout)

    def _create_file_paths_group_box(self):
        self.file_paths_group_box = QtWidgets.QGroupBox("Chemin des fichiers")
        layout = QtWidgets.QGridLayout()

        layout.addWidget(
            QtWidgets.QLabel("Csv des codes INSEE en entrée :"), 0, 0
        )
        self.input_csv_combobox = QtWidgets.QComboBox()
        layout.addWidget(self.input_csv_combobox, 0, 1)
        browse_button_csv = QtWidgets.QPushButton("Chercher...")
        browse_button_csv.clicked.connect(self.browse_to_select_input_csv)
        layout.addWidget(browse_button_csv, 0, 2)

        layout.addWidget(QtWidgets.QLabel("Dossier du téléchargement :"), 1, 0)
        self.output_download_dir_combobox = QtWidgets.QComboBox()
        layout.addWidget(self.output_download_dir_combobox, 1, 1)
        browse_button_dl = QtWidgets.QPushButton("Chercher...")
        browse_button_dl.clicked.connect(self.browse_to_set_download_dir)
        layout.addWidget(browse_button_dl, 1, 2)

        self.file_paths_group_box.setLayout(layout)

    @QtCore.Slot()
    def browse_to_select_input_csv(self):
        file_picked = QtWidgets.QFileDialog.getOpenFileName(
            self, "Choisir le fichier csv d'entrée", ".", "*.csv"
        )
        self.input_csv_combobox.addItem(file_picked[0])

    @QtCore.Slot()
    def browse_to_set_download_dir(self):
        dir_picked = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Choisir le dossier du zip", "."
        )
        self.output_download_dir_combobox.addItem(dir_picked)

    def _create_buttons_group_box(self):
        self.horizontal_group_box = QtWidgets.QGroupBox("Boutons")
        layout = QtWidgets.QHBoxLayout()
        download_button = QtWidgets.QPushButton(
            "Télécharger le zip", clicked=self.download_zip
        )
        layout.addWidget(download_button)
        extract_button = QtWidgets.QPushButton(
            "Extraire les DXF", clicked=self.extract_cities
        )
        layout.addWidget(extract_button)
        convert_button = QtWidgets.QPushButton(
            "Convertir en DWG", clicked=self.convert_into_dwg
        )
        layout.addWidget(convert_button)
        self.horizontal_group_box.setLayout(layout)

    @QtCore.Slot()
    def download_zip(self):
        _, o = self.are_combo_boxes_filed()
        if o:
            wget.download(
                url=f"https://cadastre.data.gouv.fr/data/dgfip-pci-vecteur/latest/dxf-cc/departements/dep{self.spin_box.value()}.zip",
                out=self.output_download_dir_combobox.currentText(),
            )
            self.show_msg(f"dep{self.spin_box.value()}.zip est téléchargé")
        else:
            self.show_msg("La combo box du dossier de sortie est vide")

    def are_combo_boxes_filed(self):
        has_output = True
        has_input = True
        csv_path = self.input_csv_combobox.currentText()
        if csv_path == "":
            has_input = False
        output_path = self.output_download_dir_combobox.currentText()
        if output_path == "":
            has_output = False
        return has_input, has_output

    def show_msg(self, txt: str) -> None:
        msg_box = QtWidgets.QMessageBox()
        msg_box.setText(txt)
        msg_box.exec()

    @QtCore.Slot()
    def extract_cities(self):
        i, o = self.are_combo_boxes_filed()
        if i is False or o is False:
            self.show_msg("Remplir les 2 Combo box")
            return
        output_path = self.output_download_dir_combobox.currentText()
        csv_path = self.input_csv_combobox.currentText()
        zip_file_path = output_path + f"/dep{self.spin_box.value()}.zip"
        if not QtCore.QFile.exists(zip_file_path):
            self.show_msg(
                f"{zip_file_path} n'existe pas ou n'a pas été encore téléchargé"
            )
            return
        csv_file_name = QtCore.QDir(csv_path).dirName()[:-4] + "_DXF"
        self.extract_zip_in_archi(
            output_path + "/" + csv_file_name + "/", zip_file_path
        )
        self.extract_tbz_in_archi(output_path + "/" + csv_file_name + "/")
        self.show_msg("Extractions finies !")

    def extract_zip_in_archi(self, root_path, zip_file_path):
        codes = []
        names = []
        with open(self.input_csv_combobox.currentText(), "r") as f:
            for r in csv.DictReader(f, delimiter=","):
                codes.append(r["code"])
                names.append(r["city"])
        with zipfile.ZipFile(zip_file_path) as z:
            all_files_in_zip = z.namelist()
            dxf_bouliac = [
                i for i in all_files_in_zip if i.startswith(tuple(codes))
            ]
            for i in dxf_bouliac:
                z.extract(i, root_path)
        for d in os.listdir(root_path):
            os.rename(root_path + d, root_path + names[codes.index(d)])

    def extract_tbz_in_archi(self, root_path):
        for city_folder in os.listdir(root_path):
            for tbz_file in os.listdir(root_path + city_folder):
                with tarfile.open(
                    root_path + city_folder + "/" + tbz_file
                ) as t:
                    t.extractall(root_path + city_folder)
                os.remove(root_path + city_folder + "/" + tbz_file)

    @QtCore.Slot()
    def convert_into_dwg(self) -> None:
        i, o = self.are_combo_boxes_filed()
        if i is False or o is False:
            self.show_msg("Remplir les 2 Combo box")
            return
        qdir = QtCore.QDir("C:/Program Files/ODA/")

        ODA_PATH = (
            "C:/Program Files/ODA/"
            + [
                e
                for e in qdir.entryList(filters=QtCore.QDir.AllDirs)
                if e.startswith("ODAFileConverter")
            ][0]
            + "/ODAFileConverter.exe"
        )
        if QtCore.QFileInfo.exists(ODA_PATH) is False:
            self.show_msg(
                "Vous n'avez pas ODA, le télécharger sur https://www.opendesign.com/guestfiles/oda_file_converter"
            )
            return
        INPUT_FOLDER = (
            self.output_download_dir_combobox.currentText()
            + "/"
            + QtCore.QDir(self.input_csv_combobox.currentText()).dirName()[:-4]
            + "_DXF/"
        )
        if QtCore.QFileInfo.exists(INPUT_FOLDER) is False:
            self.show_msg(
                f"{INPUT_FOLDER} n'existe pas encore, veuillez faire l'extraction"
            )
            return
        OUTPUT_FOLDER = (
            self.output_download_dir_combobox.currentText()
            + "/"
            + QtCore.QDir(self.input_csv_combobox.currentText()).dirName()[:-4]
            + "_DWG/"
        )
        OUTVER = "ACAD2018"
        OUTFORMAT = "DWG"
        RECURSIVE = "1"
        AUDIT = "1"
        INPUTFILTER = "*.DXF"

        subprocess.run(
            [
                ODA_PATH,
                INPUT_FOLDER,
                OUTPUT_FOLDER,
                OUTVER,
                OUTFORMAT,
                RECURSIVE,
                AUDIT,
                INPUTFILTER,
            ],
            shell=True
        )


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    w = Dialog()
    w.show()
    sys.exit(app.exec())
